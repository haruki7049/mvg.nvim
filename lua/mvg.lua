-- A function which generate a metasyntactic variable by a list which contains some metasyntactic variable, hoge, fuga, foo, and etc
local function mvg()
  local mvg_list = {
    "hoge",
    "fuga",
    "foo",
    "bar",
    "baz",
    "qux",
    "quux",
    "corge",
    "grault",
    "garply",
    "waldo",
    "fred",
    "plugh",
    "xyzzy",
    "thud",
  }
  print(math.random(1, #mvg_list))
end

local function setup(terminal, executors)
  vim.api.nvim_create_user_command("MVG", function()
    mvg()
  end, {})
end

return {
  setup = setup,
}
